#! /usr/bin/env python3
# coding: utf-8

import json
import math
import re
from pathlib import Path

from .config_off import *


class Parser:
    """
    Read and filter JSON.
    Generate path of files for products pages.
    """

    def __init__(self):
        self.json_input = None
        self.keyword = None
        self.filtered = None
        self.categories_name = []
        self.re_filter = re.compile(r"%")

    @staticmethod
    def is_valid(keys, dico, refilter):
        """
        Check if a key have None, empty.
        """
        is_valid = True
        for k in keys:
            if k == "url" and refilter.search(dico[k]) is not None:
                is_valid = False
                break

            if dico[k] is None or dico[k] == "":
                is_valid = False
                break

            if k == 'nutriments':
                if not 'energy' in dico[k]:
                    is_valid = False
                    break
                elif not dico[k]['energy']:
                    is_valid = False
                    break

        return is_valid

    def path_constructor(self, data, directory):
        """
        Put filename and path of json files in list.
        """
        count = 0
        files_list = []
        for categorie in data:
            pages_to_get = math.ceil(PRODUCTS_NUMBER / 20)
            for page in range(1, (pages_to_get + 1)):
                url = categorie["url"] + "/" + str(page) + ".json"
                data = (url.lstrip("https://")).replace('/', '')
                files_list.append(directory + data)
                name = categorie["name"]
                self.categories_name.append(name)
            count += 1
            if count == (CATEGORIES_NUMBER + 1):
                break
        return files_list

    def parse_files(self, files_list):
        """
        Parse a list of files.
        """
        filtered_json = []
        for files in files_list:
            filtered_json += self.parse_json(files, KEYWORD_PROD)
        return filtered_json

    def parse_json(self, data_input, keyword_set):
        """
        Get some useful data from defined keyword in config.py.
        Keep only data that match keyword.
        """
        self.json_input = data_input
        self.keyword = keyword_set

        with open(self.json_input, "r", encoding="utf8") as data:
            data_json = json.load(data)

        # Read firt level of json structure
        count = 0  # only for test purpose
        filtered_list = []
        filtered_dict = {}
        for key in data_json:
            try:
                for nested_dict in data_json[key]:
                    # Discard incomplete categories or products
                    if self.keyword <= set(nested_dict):
                        # Keep only wanted data
                        if self.is_valid(self.keyword, nested_dict,
                                         self.re_filter):
                            filtered_dict = {}
                            for keyword in self.keyword:
                                # In specific case, keep first index of list
                                # categories_hierarchy
                                if keyword == "categories_hierarchy":
                                    filtered_dict[keyword] = \
                                        nested_dict[keyword][0]
                                else:
                                    filtered_dict[keyword] = \
                                        nested_dict[keyword]
                            filtered_list.append(filtered_dict)
                    count += 1
            except TypeError:
                continue
        return filtered_list

    def truncate(self, filtered, genre):
        """ Truncate too long stings. """
        if genre == "categories":
            for item in filtered:
                item["id"] = item["id"][:80]
                item["name"] = item["name"][:80]
                item["url"] = item["url"][:255]
        if genre == "products":
            for item in filtered:
                item["id"] = item["id"][:80]
                item["product_name"] = item["product_name"][:80]
                item["brands"] = item['brands'][:80]
                item["stores"] = item['stores'][:80]
                item["url"] = item['url'][:255]
        return filtered
