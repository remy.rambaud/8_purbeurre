#! /usr/bin/env python3
# coding: utf-8

"""
This module can get JSON data from Open Food Facts
and parse it with Parser

You have to set the numbers of products to keep in database
with PRODUCTS_NUMBER in config.py
"""
import json
import math
import os
import logging

from pathlib import Path

import requests

from .config_off import PRODUCTS_NUMBER, CATEGORIES_NUMBER

logger_json = logging.getLogger('init_data.json')

class Json:
    """
    Request json from Open Food Facts.
    """

    def __init__(self):
        self.json_output = ""
        self.url = ""

    def request(self, directory, data, url):
        """
        Get json file from OFF.
        """
        self.json_output = directory + data
        self.url = url
        data_json = Path(self.json_output)

        if not Path(directory).exists():
            os.makedirs(directory)

        if not data_json.exists():
            with open(self.json_output, "w", encoding="utf8") as data:
                response = requests.get(self.url)
                output_json = json.loads(response.text)
                json.dump(output_json, data, ensure_ascii=False, indent=2)

    def request_url_list(self, data, directory):
        """
        Construct URL and download json.
        """
        count = 0
        for key in data:
            pages_to_get = math.ceil(PRODUCTS_NUMBER / 20)
            for page in range(1, (pages_to_get + 1)):
                url = key["url"] + "/" + str(page) + ".json"
                data = (url.lstrip("https://")).replace('/', '')
                logger_json.debug(data)
                self.request(directory, data, url)
            count += 1
            if count == (CATEGORIES_NUMBER + 1):
                break
