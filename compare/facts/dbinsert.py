#! /usr/bin/env python3
# coding: utf-8

"""
This module manage MySQL database.
"""

from django.db.models import Count
from compare.models import Categorie

class DbInsert:
    """
    Insert data in MySQL database.
    """

    def __init__(self):
        self.connect = "dbauth"

    def insert_categories(self, categories_list):
        """
        Insert parsed data from json.
        """
        for categorie in categories_list:
            try:
                c = Categorie(
                    id_categorie=categorie["id"],
                    name=categorie["name"],
                    url=categorie["url"],
                    products_nb=categorie["products"]
                )
                c.save()
            except:
                continue

    def insert_products(self, products_list):
        """
        Insert parsed data from json.
        """

        for product in products_list:
            # print(product["categories_hierarchy"][0])
            try:
                p = Categorie.objects.get(pk=product["categories_hierarchy"])

                p.product_set.create(
                    id_product=product["id"],
                    product_name=product["product_name"],
                    nutrition_grade_fr=product["nutrition_grade_fr"],
                    energy=product["nutriments"]["energy"],
                    brands=product["brands"],
                    stores=product["stores"],
                    url=product["url"],
                    image_url=product['image_url'],
                    categorie=product["categories_hierarchy"][0]
                )
                p.save()
            except:
                continue

    @staticmethod
    def unused_categories():
        """
        Delete categories with 1 product
        """

        Categorie.objects.annotate(
            prodcnt=Count('product__id_product')
        ).order_by('-prodcnt').filter(prodcnt__lte=1).delete()
