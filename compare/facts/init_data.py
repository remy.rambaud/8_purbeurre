#! /usr/bin/env python3
# coding: utf-8

"""
Subtitute, with Open Food Facts database.
Find a product's substitute.
"""

import os
import sys
import logging

import compare.facts

from .config_off import DIR_CAT, FIL_CAT, URL_CAT, KEYWORD_CAT


def initialize():
    """
    Download, parse and insert data in MySQL.
    """

    # Create logger
    logger = logging.getLogger('init_data')
    logger.setLevel(logging.DEBUG)

    # Create handlers
    current_dir = os.path.dirname(os.path.abspath(__file__))
    log_file = os.path.join(current_dir, 'init_data.log')
    log_file = logging.FileHandler(log_file, 'w')
    log_file.setLevel(logging.DEBUG)

    log_console = logging.StreamHandler()
    log_console.setLevel(logging.INFO)

    # Create formatter
    formatter = logging.Formatter(
        '%(asctime)s %(filename)s %(levelname)s %(message)s',
        '%m/%d/%Y %I:%M:%S %p')

    # Add formatter
    log_console.setFormatter(formatter)
    log_file.setFormatter(formatter)

    # Add handlers to logger
    logger.addHandler(log_console)
    logger.addHandler(log_file)

    # Get json files from the OpenFactsFoods API and parse them.
    logger.info("Get categories json.")
    get_json = compare.facts.json_off.Json()
    get_json.request(DIR_CAT, FIL_CAT, URL_CAT)

    categories_parser = compare.facts.parser_off.Parser()
    categories = categories_parser.parse_json(
        DIR_CAT + FIL_CAT,
        KEYWORD_CAT
    )
    categories = categories_parser.truncate(categories, "categories")

    logger.info("Get products json.")
    get_json.request_url_list(categories, DIR_CAT)

    logger.info("Parse products json.")
    files_list = categories_parser.path_constructor(categories, DIR_CAT)
    products_parser = compare.facts.parser_off.Parser()
    products = products_parser.parse_files(files_list)
    products = products_parser.truncate(products, "products")
    logger.debug([p['nutriments']['energy'] for p in products])

    logger.info("Insert categories in database.")
    insert_cat = compare.facts.dbinsert.DbInsert()
    insert_cat.insert_categories(categories)

    logger.info("Insert products in database.")
    insert_prod = compare.facts.dbinsert.DbInsert()
    insert_prod.insert_products(products)
    insert_prod.unused_categories()


def clear():
    """
    Clear screen.
    """
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")


def __main__():
    for arg in sys.argv:
        if arg == "--init":
            print("First start... \n")
            initialize()
            print("First start finished!")


if __name__ == "__main__":
    __main__()
