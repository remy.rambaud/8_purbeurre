import os

""" Constants and configuration """

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))

URL_CAT = "https://fr.openfoodfacts.org/categories.json"
DIR_CAT = os.path.join(CURRENT_DIR, 'data/')
FIL_CAT = 'categories.json'

# Products by categories
PRODUCTS_NUMBER = 500

# Number of categories
CATEGORIES_NUMBER = 60

# Filters
KEYWORD_CAT = {'id', 'name', 'url', 'products'}
KEYWORD_PROD = {
    'product_name', 'url', 'nutrition_grade_fr',
    'id', 'brands', 'stores', 'categories_hierarchy', 'nutriments',
    'image_url'
}
