#! /usr/bin/env python3
# coding: utf-8

"""
This module manage MySQL database.
"""
from random import sample
from django.db.models import Count

from compare.models import Categorie, Product


class DbRead:
    """
    Get and search data in MySQL database.
    """

    def __init__(self):
        self.categories = ''

    def get_categories(self):
        """
        Get categories with more than 1 product.
        """
        self.categories = Categorie.objects.annotate(
            prodcnt=Count('product__id_product')
        ).order_by('-prodcnt').filter(prodcnt__gt=1)

        return self.categories

    def get_products_list(self):
        """
        Get list of products.
        """
        products_list = []
        if self.categories != '':
            for categorie_name in self.categories:
                products = Product.objects.filter(
                    categorie__name__icontains=categorie_name)
                products_list += [prod.product_name for prod in products]
        else:
            print("Call get_categories first.")
            return None

        return products_list

    @staticmethod
    def search_product(user_query):
        """
        Get a list of products according user's query.
        """
        return Product.objects.filter(product_name__icontains=user_query)

    @staticmethod
    def get_substitute(user_query, *user_filter):
        """
        Get substitute for a product.
        """
        product_selected = Product.objects.get(id_product__contains=user_query)
        product_grade = product_selected.nutrition_grade_fr
        product_energy = product_selected.energy
        product_categorie = product_selected.categorie.id_categorie

        filter_query = {
            'categorie__id_categorie__exact': product_categorie,
        }

        if 'energy' in user_filter:
            filter_query.update({'energy__lt': product_energy})

        if 'grade' in user_filter:
            filter_query.update({'nutrition_grade_fr__lt': product_grade})

        product_candidates = Product.objects.filter(
            **filter_query
        ).order_by('nutrition_grade_fr').exclude(id_product__exact=user_query)

        if not product_candidates:
            product_substitute = ''
        elif len(product_candidates) == 1:
            product_substitute = [product_candidates[0], ]
        else:
            # product_substitute = choice(product_candidates)
            product_substitute = sample(list(product_candidates), k=6)

        return product_selected, product_substitute
