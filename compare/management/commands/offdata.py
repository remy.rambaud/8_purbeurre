from django.core.management.base import BaseCommand, CommandError

from compare.facts.init_data import initialize


class Command(BaseCommand):
    help = 'Download new set of data from Open Food Facts'

    def handle(self, *args, **options):
        self.stdout.write("Launch init_data", ending='\n')
        initialize()