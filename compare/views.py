import logging

from random import choice

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect, render
from django.http import Http404

from compare.facts import DbRead

from .forms import Sign
from .models import Bookmark, Product

# Instance of logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Get random background
def background():
    return choice(['bg-greg-jeanneau-unsplash.jpg',
                   'bg-kawin-harasai-unsplash.jpg',
                   'bg-olenka-kotyk-unsplash.jpg', ])


def index(request):
    context = {
        'background': background(),
    }
    return render(request, 'compare/index.html', context)


def results(request):
    # Clear checkbox session variables
    request.session['grade'] = None
    request.session['energy'] = None

    query_result = DbRead.search_product(request.POST['product'])

    if 'grade' in request.POST:
        request.session['grade'] = request.POST['grade']
    if 'energy' in request.POST:
        request.session['energy'] = request.POST['energy']

    context = {
        'query': query_result,
        'background': background(),
    }

    return render(request, 'compare/results.html', context)


def details(request, product):
    user_filter = (request.session.get('grade'), request.session.get('energy'))

    try:
        product_selected, product_substitute = DbRead.get_substitute(
            product, *user_filter)
    except Product.DoesNotExist:
        raise Http404

    context = {
        'product': product_substitute,
        'product_selected': product_selected,
        'background': background(),
    }

    logger.info('Product selected', extra={
        'product_selected': product_selected,
        'product_substitute': product_substitute,
    })

    if not product_substitute:
        context['user_result'] = 'Pas de substitut trouvé pour ce produit.'

    return render(request, 'compare/details.html', context)


def signup(request):
    form = Sign()
    context = {
        'form': form,
        'background': background(),
    }

    if request.method == 'POST':
        form = Sign(request.POST)

        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            if not User.objects.filter(username=email):
                user = User.objects.create_user(email, email, password)

                # Authenticate and login user after registration.
                user = authenticate(username=form.cleaned_data['email'],
                                    password=form.cleaned_data['password'],)

                login(request, user)
                context['user_result'] = "Le compte a été créé !"
                return render(request, 'compare/form_signin.html', context)

            else:
                context['user_result'] = "L'utilisateur existe déjà."

        else:
            context = {'form': form}

    return render(request, 'compare/form_signup.html', context)


def signin(request):
    form = Sign()

    nextvalue = request.GET.get('next')

    context = {
        'form': form,
        'background': background(),
        'next': nextvalue,
    }

    if request.user.is_authenticated and nextvalue is not None:
        return redirect(nextvalue)

    else:
        if request.method == 'POST':
            form = Sign(request.POST)
            if form.is_valid():
                user = authenticate(username=form.cleaned_data['email'],
                                    password=form.cleaned_data['password'],)
                if user is not None:
                    login(request, user)
                    if nextvalue is not None:
                        return redirect(nextvalue)
                    else:
                        return redirect('compare:account')
                else:
                    context['user_result'] = 'Identification invalide.'
                    return render(request, 'compare/form_signin.html', context)
            else:
                context = {
                    'form': form,
                    'user_result': 'Identification invalide.',
                    'next': nextvalue,
                }

    return render(request, 'compare/form_signin.html', context)


def signout(request):
    if not request.user.is_authenticated:
        return redirect('compare:signin')
    logout(request)
    return redirect('compare:index')


@login_required(login_url='/compare/signin/')
def account(request):

    user_authenticated = User.objects.get(pk=request.user.id)
    product_in_bookmark = Bookmark.objects.filter(
        user=user_authenticated)

    context = {
        'user_result': 'Vous êtes déconnecté⋅e.',
        'products': product_in_bookmark,
        'background': background(),
    }

    return render(request, 'compare/account.html', context)


@login_required(login_url='/compare/signin/')
def bookmark(request, product):

    context = {
        'background': background(),
    }

    user_authenticated = User.objects.get(pk=request.user.id)
    product_subtitute = Product.objects.get(id_product=product)

    product_in_bookmark = Bookmark.objects.filter(
        user=user_authenticated
    ).filter(
        id_result=product_subtitute
    ).exists()

    if not product_in_bookmark:
        add_bookmark = Bookmark(
            id_result=product_subtitute, user=user_authenticated)
        add_bookmark.save()
        return redirect('compare:account')
    else:
        context['user_result'] = "Ce produit a déjà été\
             enregristré dans les favoris."

    return render(request, "compare/details.html", context)


def product_page(request, product):
    get_product = get_object_or_404(Product, id_product=product)

    context = {
        'product': get_product,
        'background': background(),
    }

    return render(request, 'compare/product.html', context)


def mentions(request):
    context = {
        'background': background(),
    }

    return render(request, 'compare/mentions.html', context)
