from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from .models import Bookmark, Categorie, Product


def create_data():
    """ Create data in test database """
    products_list = [
        {
            'id_product': '1',
            'product_name': 'orange',
            'nutrition_grade_fr': 'b',
            'categorie': 'someid',
            'energy': 1500
        },
        {
            'id_product': '2',
            'product_name': 'citron',
            'nutrition_grade_fr': 'a',
            'categorie': 'someid',
            'energy': 1600
        },
        {
            'id_product': '3',
            'product_name': 'tomate',
            'nutrition_grade_fr': 'c',
            'categorie': 'someid',
            'energy': 1000
        },
    ]

    Categorie.objects.create(id_categorie='someid', name='somename')

    for product in products_list:
        prod = Categorie.objects.get(pk='someid')
        prod.product_set.create(
            **product
        )


def create_user():
    User.objects.create_user('john@sign.in', 'john@sign.in', 'smith')


def signin_user(self):
    self.client.login(username='john@sign.in', password='smith')


def signout_user(self):
    self.client.logout()


class CompareIndexTests(TestCase):
    def test_get_index(self):
        response = self.client.get(reverse('compare:index'))
        response_redirect = self.client.get('/')

        self.assertEqual(response.status_code, 200)
        # Redirect from root projet : code 302
        self.assertEqual(response_redirect.status_code, 302)


class CompareResultsTests(TestCase):
    def setUp(self):
        create_data()

    def test_search(self):
        response = self.client.post(reverse('compare:results'), {
            'product': 'orange'})
        self.assertEqual(
            response.context['query'][0].product_name,
            'orange')


class CompareDetailsTests(TestCase):
    def setUp(self):
        create_data()

    def test_404substitute(self):
        response = self.client.get('/compare/4/details/')
        self.assertEqual(response.status_code, 404)

    def test_nosubstitute(self):
        session = self.client.session
        session['grade'] = 'grade'
        session.save()
        response = self.client.get('/compare/2/details/')
        self.assertContains(
            response, 'Pas de substitut trouvé pour ce produit.')

    def test_grade_comparison(self):
        session = self.client.session
        session['grade'] = 'grade'
        session.save()
        response = self.client.get('/compare/1/details/')
        self.assertEqual(response.context['product'][0].product_name, 'citron')

    def test_energy_comparison(self):
        session = self.client.session
        session['energy'] = 'energy'
        session.save()
        response = self.client.get('/compare/1/details/')
        self.assertEqual(response.context['product'][0].product_name, 'tomate')


class CompareSignTests(TestCase):
    def setUp(self):
        create_user()

    def tearDown(self):
        signout_user(self)
        User.objects.all().delete()

    def test_signup(self):
        user = {'email': 'john@sign.up', 'password': 'smith'}
        response = self.client.post(reverse('compare:signup'), user)
        self.assertEqual(str(response.context['user']), 'john@sign.up')

    def test_signin(self):
        user = {'email': 'john@sign.in', 'password': 'smith'}
        response = self.client.post(reverse('compare:signin'), user)
        self.assertEqual(response.url, '/compare/account/')


class CompareSignoutTests(TestCase):
    def setUp(self):
        create_user()

    def tearDown(self):
        signout_user(self)
        User.objects.all().delete()

    def test_redirect_status_logged(self):
        signin_user(self)
        response = self.client.get('/compare/signout/')
        self.assertEqual(response.status_code, 302)

    def test_redirect_url_logged(self):
        signin_user(self)
        response = self.client.get('/compare/signout/')
        self.assertRedirects(response, '/compare/')

    def test_redirect_status_unlogged(self):
        response = self.client.get('/compare/signout/')
        self.assertEqual(response.status_code, 302)

    def test_redirect_url_unlogged(self):
        response = self.client.get('/compare/signout/')
        self.assertRedirects(response, '/compare/signin/')


class CompareBookmarkTests(TestCase):
    def setUp(self):
        create_data()
        create_user()

    def tearDown(self):
        signout_user(self)
        User.objects.all().delete()
        Categorie.objects.all().delete()
        Bookmark.objects.all().delete()

    def test_redirect_logged(self):
        signin_user(self)
        response = self.client.get('/compare/1/bookmark/')
        bookmark = Bookmark.objects.first()
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/compare/account/')
        self.assertEqual(str(bookmark), 'Bookmark object (1)')

    def test_redirect_unlogged(self):
        response = self.client.get('/compare/1/bookmark/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/compare/signin/?next=/compare/1/bookmark/')


class CompareAccountTests(TestCase):
    def setUp(self):
        create_data()
        create_user()
        self.user = User.objects.first()
        self.product = Product.objects.first()
        self.bookmark = Bookmark.objects.create(
            id=1, id_result=self.product, user=self.user)

    def tearDown(self):
        signout_user(self)
        User.objects.all().delete()
        Categorie.objects.all().delete()
        Bookmark.objects.all().delete()

    def test_logged(self):
        signin_user(self)
        response = self.client.get('/compare/account/')
        self.assertEqual(response.status_code, 200)

    def test_unlogged(self):
        response = self.client.get('/compare/account/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/compare/signin/?next=/compare/account/')

    def test_get_product(self):
        signin_user(self)
        response = self.client.get('/compare/account/')
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.context['products'])


class CompareProductTests(TestCase):
    def setUp(self):
        create_data()

    def tearDown(self):
        Product.objects.all().delete()

    def test_show_product(self):
        response = self.client.get('/compare/1/product/')
        product = response.context['product']
        self.assertEqual(product.product_name, 'orange')
