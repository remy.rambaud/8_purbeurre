from django.urls import path

from . import views

app_name = 'compare'
urlpatterns = [
    path('', views.index, name='index'),
    path('results/', views.results, name='results'),
    path('<product>/details/', views.details, name='details'),
    path('signup/', views.signup, name='signup'),
    path('signin/', views.signin, name='signin'),
    path('signout/', views.signout, name='signout'),
    path('account/', views.account, name='account'),
    path('<product>/bookmark/', views.bookmark, name='bookmark'),
    path('<product>/product/', views.product_page, name='product'),
    path('mentions/', views.mentions, name='mentions'),
]
