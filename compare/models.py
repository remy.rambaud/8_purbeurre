from math import ceil

from django.contrib.auth.models import User
from django.db import models


class Categorie(models.Model):
    id_categorie = models.CharField(max_length=80, primary_key=True)
    name = models.CharField(max_length=80, null=True)
    url = models.CharField(max_length=255, null=True)
    products_nb = models.IntegerField(null=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    id_product = models.CharField(max_length=80, primary_key=True)
    product_name = models.CharField(max_length=80, null=True)
    nutrition_grade_fr = models.CharField(
        "nutriscore", max_length=1, null=True)
    energy = models.FloatField(max_length=10, null=True)
    brands = models.CharField(max_length=80, null=True)
    stores = models.CharField(max_length=80, null=True)
    url = models.URLField(max_length=255, null=True)
    image_url = models.URLField(max_length=255, null=True)
    categorie = models.ForeignKey("Categorie", on_delete=models.CASCADE)

    def kcal(self):
        return ceil(self.energy / 4.184)

    def __str__(self):
        return self.product_name


class Bookmark(models.Model):
    id_result = models.ForeignKey("Product", on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
