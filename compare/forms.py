from django import forms


class Sign(forms.Form):
    email = forms.EmailField(
        label='Email',
        widget=forms.EmailInput(
            attrs={'class': 'form-control', 'placeholder': 'Adresse e-mail'}),
        required=True,
    )

    password = forms.CharField(
        label='Mot de passe',
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder': 'Mot de passe'}),
        required=True)
