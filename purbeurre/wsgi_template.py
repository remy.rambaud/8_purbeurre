"""
WSGI config for purbeurre project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

#                                           #
# Remind to put this file int .gitignore    #
#                                           #

from django.core.wsgi import get_wsgi_application
import os

os.environ['ENV'] = 'NAME OF ENV'
os.environ['DJANGO_SECRET_KEY'] = 'PUT SECRET KEY HERE'
os.environ['DATABASE_URL'] = 'postgres://LOGIN:PASSWORD@HOST:5432/DATABASENAME'

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'purbeurre.settings')

application = get_wsgi_application()
